<?php

$factory->define(App\QuestionOption::class, function (Faker\Generator $faker) {
    return [
        "option_text" => $faker->name,
        "correct" => 0,
    ];
});
