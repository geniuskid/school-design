<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5cfd3186c57ccQuestionQuestionOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('question_question_option')) {
            Schema::create('question_question_option', function (Blueprint $table) {
                $table->integer('question_id')->unsigned()->nullable();
                $table->foreign('question_id', 'fk_p_312782_312783_questi_5cfd3186c59b1')->references('id')->on('questions')->onDelete('cascade');
                $table->integer('question_option_id')->unsigned()->nullable();
                $table->foreign('question_option_id', 'fk_p_312783_312782_questi_5cfd3186c5ab1')->references('id')->on('question_options')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_question_option');
    }
}
