<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuestionOption
 *
 * @package App
 * @property text $option_text
 * @property tinyInteger $correct
*/
class QuestionOption extends Model
{
    use SoftDeletes;

    protected $fillable = ['option_text', 'correct'];
    protected $hidden = [];
    
    
    
    public function question()
    {
        return $this->belongsToMany(Question::class, 'question_question_option')->withTrashed();
    }
    
}
