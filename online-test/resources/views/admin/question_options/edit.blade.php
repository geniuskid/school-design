@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.question-options.title')</h3>
    
    {!! Form::model($question_option, ['method' => 'PUT', 'route' => ['admin.question_options.update', $question_option->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('question', trans('quickadmin.question-options.fields.question').'', ['class' => 'control-label']) !!}
                    <button type="button" class="btn btn-primary btn-xs" id="selectbtn-question">
                        {{ trans('quickadmin.qa_select_all') }}
                    </button>
                    <button type="button" class="btn btn-primary btn-xs" id="deselectbtn-question">
                        {{ trans('quickadmin.qa_deselect_all') }}
                    </button>
                    {!! Form::select('question[]', $questions, old('question') ? old('question') : $question_option->question->pluck('id')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple', 'id' => 'selectall-question' ]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question'))
                        <p class="help-block">
                            {{ $errors->first('question') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('option_text', trans('quickadmin.question-options.fields.option-text').'*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('option_text', old('option_text'), ['class' => 'form-control ', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('option_text'))
                        <p class="help-block">
                            {{ $errors->first('option_text') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('correct', trans('quickadmin.question-options.fields.correct').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('correct', 0) !!}
                    {!! Form::checkbox('correct', 1, old('correct', old('correct')), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('correct'))
                        <p class="help-block">
                            {{ $errors->first('correct') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script>
        $("#selectbtn-question").click(function(){
            $("#selectall-question > option").prop("selected","selected");
            $("#selectall-question").trigger("change");
        });
        $("#deselectbtn-question").click(function(){
            $("#selectall-question > option").prop("selected","");
            $("#selectall-question").trigger("change");
        });
    </script>
@stop