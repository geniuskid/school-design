<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
      <header id="site-header" class="header" role="banner" aria-label="Main Header">
        <div class="centering">
          <div class="header-logo">
            <a href="{{ url('/') }}" title="Back to Homepage">
              SCHOOL MANAGEMENT
            </a>
            @guest
            <a href="{{ route('login') }}">{{ __('Login') }}</a>

            @else
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @endguest

          </div>


          <a class="hamburger" href="#" title="Menu">
            <span class="line-1"></span>
            <span class="line-2"></span>
            <span class="line-3"></span>
          </a>

          <nav id="main-nav" class="main-nav" role="navigation" aria-label="Main Navigation">
            <ul class="menu">
              <li class="menu-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="menu-item"><a href="admission.html">Admission</a></li>
              <li class="menu-item"><a href="report.html">Report</a></li>
              <li class="menu-item"><a href="appointment.html">Appointment</a></li>
              <li class="menu-item"><a href="tets.html">Test</a></li>
              <li class="menu-item"><a href="apply-job.html">Apply Job</a></li>
              <li class="menu-item"><a href="student-records.html">Student Records</a></li>
              <li class="menu-item"><a href="be-franchise.html">Be franchise</a></li>
            </ul>
          </nav>
        </div>
      </header>


        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
