@extends('layouts.app')

@section('content')
<section class="home">
  <div class="centering">
    <div class="grid">
      <div class="grid-xs-12 grid-md-3">
        <div class="sidebar">
          <h2>Up Coming News</h2>
          <div class="grid">
            <div class="grid-xs-12">
              <div class="item-blog">
                <img src="https://via.placeholder.com/150" alt="">
                <h2><a href="#">News Title</a></h2>
              </div>
            </div>
            <div class="grid-xs-12">
              <div class="item-blog">
                <img src="https://via.placeholder.com/150" alt="">
                <h2><a href="#">News Title</a></h2>
              </div></div>
            <div class="grid-xs-12">
              <div class="item-blog">
                <img src="https://via.placeholder.com/150" alt="">
                <h2><a href="#">News Title</a></h2>
              </div></div>
    
          </div>
        </div>
      </div>
      <div class="grid-xs-12 grid-md-9">
          <div class="hero">
            <div class="centering">
              <h1>WELCOME TO SCHOOL MANAGEMENT</h1>
            </div>
          </div>
          <div class="grid">
            <div class="grid-xs-12 grid-md-4">
              <iframe width="100%" height="315" src="https://www.youtube.com/embed/2LOkI3Xyd_E?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="grid-xs-12 grid-md-4">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/2LOkI3Xyd_E?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="grid-xs-12 grid-md-4">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/2LOkI3Xyd_E?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
          </div>
      </div>
    </div>
  </div>
</section>
@endsection
