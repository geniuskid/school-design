<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->string('country');
            $table->integer('user_id')->unsigned();
            $table->string('state');
            $table->string('city');
            $table->string('program');
            $table->string('sub_program');
            $table->date('dob');
            $table->string('sex');
            $table->string('religion');
            $table->string('center');
            $table->string('folder_path');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_forms');
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('folder_path');
        });
    }
}
