jQuery(document).ready(function($){

	// Hamburger
	$('.hamburger').on('click', function(event){
		event.preventDefault();

		$(this).toggleClass('is-active');
		$('html, body').toggleClass('is-noscroll is-open-menu');
		$('#main-nav').toggleClass('is-open-menu');
	});

	// handle main nav dropdown menu functionality
	$('.main-nav .menu-item-has-children > a').on('click',function(event){
		event.preventDefault();
		event.stopPropagation();

		var this_menu = $(this).parent(),
			this_open = $(this).parent().hasClass('open-submenu');
		// check if this submenu is open
		if(this_open) {
			// remove classes from the current open sub-menu <li>
			$(this).parent().removeClass('open-submenu current-open-menu');
			// find the closest open sub-menu <li> in the parent tree and add current class to it
			$(this).closest('.open-submenu').addClass('current-open-menu');
		} else {
			// remove classes from all open sub-menu <li>
			$('.main-nav .open-submenu').removeClass('open-submenu current-open-menu');
			// find this and all parent sub-menu <li>, add open class
			$(this).parents('.menu-item-has-children').addClass('open-submenu');
			// add current class to this sub-menu <li>
			this_menu.addClass('current-open-menu');
		}
	});
	
	// Remove Navigation when clicked outside
	//$(document).mouseup(function (event) {
	//	var container = $('.main-nav');
    //
	//	if (!container.is(event.target) && container.has(event.target).length === 0) {
	//		container.children('.menu-item').removeClass('open-submenu current-open-menu');
	//	}
	//});

	// $('.slideshow').flickity({
	// 	cellSelector: '.slideshow-slide',
	// 	accessibility: false,
	// 	cellAlign: 'center',
	// 	draggable: true,
	// 	autoPlay: true,
	// 	prevNextButtons: false,
	// 	pageDots: false
	// });

});